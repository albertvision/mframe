<?php

$config['autoload'] = FALSE; // TRUE or FALSE
$config['driver'] = 'native'; // "native" or "database"

$config['sess_name'] = '_sess'; // Session name
$config['sess_name_close_expire'] = '_auth'; // Session name of the native session, created when you use DB Session driver and you want to close session after browser closing.
$config['max_life_time'] = 0; // Session max lifetime
$config['path'] = '/MFrame'; // Session path
$config['domain'] = ''; // Session domain
$config['secure'] = false; // Secured session?
$config['db_table'] = 'sessions'; // Database table

$config['custom_drivers'] = [
    //'custom_session'=>'MFrame\CustomSession'
];

return $config;
?>