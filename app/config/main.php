<?php

$config = [
    'debug' => TRUE,
    'logging' => TRUE,
    'default_lang' => 'bg'
];

return $config;
