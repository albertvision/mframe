<?php

/**
 * MFrame uses password_hash() function provided by PHP. 
 * Read more: http://www.php.net/manual/en/function.password-hash.php
 */
$config['algorithm'] = PASSWORD_DEFAULT;
$config['options'] = [
    'salt' => 'this_is_my_secret_salt',
    'cost' => 10
];

return $config;
?>