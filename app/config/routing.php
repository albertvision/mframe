<?php

$config['dispatcher'] = 'MFrame\Routing::mainDispatcher';
$config['routes'] = [
    //'login/(referal?)' => 'DefaultController/login/$1',
];
$config['defaults'] = [
    '*' => [
        'controller' => 'DefaultController',
        'method' => 'index'
    ],
    'admin' => [
        'controller' => 'HomePage',
        'method' => 'home'
    ]
];

$config['custom_regex'] = [
    'referal' => '[0-9]+'
];

return $config;
