<?php

return [
    'min'=>'The :field field is too short! Min. length: :min symbols.',
    'max'=>'The :field field is too long! Min. length: :max symbols.',
    'email'=>'The :field field is not a valid email!',
    'integer'=>'The :field field is not an integer!',
    'ip'=>'The :field field is not a valid IPv4!',
    'numeric'=>'The :field field is not a valid number',
    'required'=>'The :field field is required!',
    'url'=>'The :field field is not a valid URL!',
    'string'=>'The :field field is not a valid string!',
    'in' => 'The :field is invalid.',
    'not_in' => 'The :field is invalid',
    'only_char' => 'The :field does not contain only text.',
    'equal_to' => 'The :field is not equal to :field2',
];
?>