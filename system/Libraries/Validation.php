<?php

namespace MFrame\Libraries;

/**
 * Validation class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2014 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core.Libraries
 */
class Validation {

    /**
     * Available rules
     * @var array
     */
    private static $_availableRules = [
        'min' => 'MFrame\Libraries\Validation::checkMin',
        'max' => 'MFrame\Libraries\Validation::checkMax',
        'email' => 'MFrame\Libraries\Validation::isEmail',
        'integer' => 'MFrame\Libraries\Validation::isInt',
        'ip' => 'MFrame\Libraries\Validation::isIP',
        'numeric' => 'MFrame\Libraries\Validation::isNum',
        'required' => 'MFrame\Libraries\Validation::isRequired',
        'url' => 'MFrame\Libraries\Validation::isURL',
        'string' => 'MFrame\Libraries\Validation::isString',
        'in' => 'MFrame\Libraries\Validation::inArray',
        'arr_vals_in' => 'MFrame\Libraries\Validation::arrayValuesInArray',
        'not_in' => 'MFrame\Libraries\Validation::notInArray',
        'only_char' => 'MFrame\Libraries\Validation::onlyChar',
        'equal_to' => 'MFrame\Libraries\Validation::equalTo',
        'captcha' => 'MFrame\Libraries\Validation::captcha',
        'date' => 'MFrame\Libraries\Validation::date',
        'time' => 'MFrame\Libraries\Validation::time',
    ];

    /**
     * Default format of the messages
     * 
     * 
     * ['border'] - Border html of the images. Ex. &lt;div class="error_messages"&gt;:messages&lt;/div&gt; 
     * 
     * ['single'] - HTML formating of a single message. Ex. &lt;p class="error"&gt;:message&lt;/p&gt;
     * 
     * @var array
     */
    private static $_defaultMessagesFormat = [
        'border' => '<ul>:messages</ul>',
        'single' => '<li>:message</li>'
    ];

    /**
     * Data to validating
     * @var array
     */
    private $_data = [];

    /**
     * Found errors
     * @var array
     */
    private $_errors = [];

    /**
     * Messages list
     * @var array
     */
    private $_messages = [];

    /**
     * Parallel validation of a field?
     * @var type 
     */
    private $_parallel = false;

    /**
     * Creates an instance of this class
     * 
     * @param array $data Data to validating
     * @param array $messages Custom messages
     * @param bool $parallelFieldValidation To make the all validations [true] of field or only one [false]
     */
    public function __construct($data = [], $messages = [], $parallelFieldValidation = false) {
        $this->_data = $data;
        $this->_messages = array_merge(Lang::get('validation'), $messages);
        $this->_parallel = $parallelFieldValidation;
    }

    /**
     * New validation
     * 
     * @param array $data Data to validating
     * @param array $messages Custom messages
     * @param bool $parallelFieldValidation To make the all validations [true] of field or only one [false]
     * @static
     * 
     * @return \MFrame\Libraries\Validation
     * @throws \Exception
     */
    public static function make($data = [], $messages = [], $parallelFieldValidation = false) {
        if (!is_array($data)) {
            throw new \Exception('Invalid data array!', 500);
        }
        if (!is_array($messages)) {
            throw new \Exception('Invalid validation messages array!', 500);
        }
        return new Validation($data, $messages, $parallelFieldValidation);
    }

    /**
     * Checks for validation failing
     * 
     * @return boolean
     */
    public function fails() {
        $data = $this->_data;
        $result = [];

        foreach ($data['data'] as $key => $value) {
            if ($this->_validateRule($key, $value, $data['rules'][$key])) {
                $result[] = true;
            } else {
                $result[] = false;
            }
        }

        if (!count($this->getMessages())) {
            return false;
        }
        return true;
    }

    /**
     * Returns error messages
     * 
     * @param string $border HTML border of ALL messages
     * @param string $single HTML border of single message
     * 
     * @return string
     */
    public function displayMessages($border = NULL, $single = NULL) {
        if (!$border) {
            $border = self::$_defaultMessagesFormat['border'];
        } if (!$single) {
            $single = self::$_defaultMessagesFormat['single'];
        }
        $border = explode(':messages', $border, 2);
        $single = explode(':message', $single, 2);

        foreach ($this->_errors as $err) {
            if ($errors) {
                $errors .= $single[1];
            }
            $errors .= str_replace(':field', $err['field'], $single[0]) . $err['message'];
        }
        $errors .= $single[1];

        return $border[0] . $errors . $border[1];
    }

    public function getMessages() {
        return $this->_errors;
    }

    /**
     * Set default message formating
     * 
     * @param string $border HTML border of ALL messages
     * @param string $single HTML border of single message
     * 
     * @return boolean
     */
    public static function setDefaultMessageFormating($border, $single = NULL) {
        if ($border) {
            self::$_defaultMessagesFormat['border'] = $border;
        } if ($single) {
            self::$_defaultMessagesFormat['single'] = $single;
        }

        return true;
    }

    /**
     * Validates elements
     * 
     * @param string $key Validation key
     * @param string $value Value
     * @param string $rules Rules
     * 
     * @return boolean
     * @throws \Exception
     */
    private function _validateRule($key, $value, $rules) {
        $return = [];
        if (trim($rules)) {
            $rules = explode('|', $rules);
            $errorFields = [];

            foreach ($rules as $rule) {
                $message = '';
                $rule_params = explode(':', $rule);
                $rule_name = array_shift($rule_params);
                $callback = self::$_availableRules[$rule_name];

                if (is_callable($callback)) {
                    foreach ($rule_params as $r) {
                        if (@preg_match("/^@(\w)+$/", $r, $match, PREG_OFFSET_CAPTURE) === 1) {
                            if ($match[0][1] === 0) {
                                $_rval = $this->_data['data'][substr($match[0][0], 1)];
                                $_rkey = array_search($r, $rule_params);
                                
                                if ($_rkey !== FALSE) {
                                    $rule_params[$_rkey] = $_rval;
                                } else {
                                    $rule_params[] = $_rval;
                                }
                                $fields[] = substr($match[0][0], 1);
                            }
                        }
                    }
                    array_unshift($rule_params, $value);
                    $result = call_user_func_array($callback, $rule_params);

                    if ($result === TRUE) {
                        $return[] = true;
                    } elseif (is_array($result)) {
                        $result['field'] = ucfirst(mb_strtolower($key));

                        if ($this->_data['messages'][$key][$rule_name]) {
                            $message = $this->_data['messages'][$key][$rule_name];
                        } else {
                            $message = $this->_messages[$rule_name];
                        }

                        if (is_array($fields) && count($fields)) {
                            $fields_count = 1;
                            foreach ($fields as $fieldKey => $fieldRealName) {
                                $result['field' . ($fieldKey + 2)] = ucfirst(mb_strtolower($fieldRealName));
                            }
                        }

                        if (preg_match_all('/:\w+/', $message, $matches, PREG_OFFSET_CAPTURE)) {
                            foreach ($matches[0] as $match) {
                                $start = strlen(substr($message, 0, $match[1]));
                                $message = substr_replace($message, $result[substr($match[0], 1)], $start, strlen($match[0]));
                            }
                        }

                        if ($this->_parallel || (!$this->_parallel && !in_array($key, $errorFields))) {
                            $this->_errors[] = [
                                'field' => $key,
                                'message' => $message
                            ];
                            if (!$this->_parallel && !in_array($key, $errorFields)) {
                                $errorFields[] = $key;
                            }
                        }
                        $return[] = false;
                    }
                } else {
                    throw new \Exception('Validation rule [' . $rule_name . '] cannot be called.', 500);
                }
            }
        } else {
            return false;
        }

        if (in_array(true, $return) && !in_array(false, $return)) {
            return true;
        }
        return false;
    }

    /**
     * List of available rules
     * 
     * @return array
     */
    public static function getRules() {
        return self::$_availableRules;
    }

    /**
     * Create new rule
     * 
     * @param string $name Rule name
     * @param string $method Callback method
     * 
     * @return boolean
     * @throws \Exception
     */
    public static function setRule($name, $method) {
        if (!is_callable($method)) {
            throw new \Exception('Method [' . $method . '] cannot be called.', 500);
        }
        self::$_availableRules[$name] = $method;

        return true;
    }

    /**
     * Checks for minimal value of a string
     * 
     * @param string $value Value
     * @param int $min Minimal value
     * 
     * @return boolean
     */
    public static function checkMin($value, $min, $required = 1) {
        $value = trim($value);
        $isMin = (strlen($value) >= $min);

        if (!$isMin && (($required !== 1 && strlen($value)) || $required === 1)) {
            return [
                'min' => $min
            ];
        }
        return true;
    }

    /**
     * Checks for maximal value of a string
     * 
     * @param string $value Value
     * @param int $max Minimal value
     * 
     * @return boolean
     */
    public static function checkMax($value, $max) {
        if (strlen(trim($value)) > $max) {
            return [
                'max' => $max
            ];
        }

        return true;
    }

    /**
     * Checks is the value is an email.
     * @param string $value Value
     * 
     * @return boolean
     */
    public static function isEmail($value) {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return [];
        }

        return true;
    }

    /**
     * Checks is the value is an integer
     * @param string $value Value
     * 
     * @return boolean
     */
    public static function isInt($value) {
        if (!is_int($value)) {
            return [];
        }

        return true;
    }

    /**
     * Checks is the value is an IP
     * @param string $value Value
     * @return boolean
     */
    public static function isIP($value) {
        if (!filter_var($value, FILTER_VALIDATE_IP)) {
            return [];
        }
        return true;
    }

    /**
     * Is numeric?
     * @param int $value Value
     * @return boolean
     */
    public static function isNum($value) {
        if (!is_numeric($value)) {
            return [];
        }
        return true;
    }

    /**
     * Is the value empty?
     * 
     * @param mixed $value Input value
     * @return boolean
     */
    public static function isRequired($value) {
        if (!$value) {
            return [];
        }
        return true;
    }

    /**
     * Is URL?
     * @param string $value Value
     * @return boolean
     */
    public static function isURL($value) {
        if (!filter_var($value, FILTER_VALIDATE_URL)) {
            return [];
        }
        return true;
    }

    /**
     * Is string?
     * 
     * @param string $value
     * @return boolean
     */
    public static function isString($value) {
        if (!is_string($value)) {
            return [];
        }
        return true;
    }

    /**
     * In array?
     * 
     * @param string $value
     * @param array|string $array
     * 
     * @return boolean
     */
    public static function inArray($value, $array = []) {
        if(is_string($array)) {
            $array = explode(',', trim($array));
        }
        if ($value === NULL || !@in_array($value, $array) || !is_array($array)) {
            return [];
        }
        return true;
    }
    
    /**
     * Search values of array in another array.
     * 
     * @param array $arr1 Array 1
     * @param array $arr2 Array to search in it
     * 
     * @return boolean
     */
    public static function arrayValuesInArray($arr1, $arr2 = []) {
        if(count(array_diff($arr1, $arr2))) {
            return [];
        }
        return true;
    }

    /**
     * It isn't array?
     * 
     * @param string $value
     * @param array|string $array
     * 
     * @return boolean
     */
    public static function notInArray($value, $array = []) {
        if(is_string($array)) {
            $array = explode(',', trim($array));
        }
        if ($array && in_array($value, $array)) {
            return [];
        }
        return true;
    }

    /**
     * Does value contain only characters
     * 
     * @param type $value
     * @return boolean
     */
    public static function onlyChar($value) {
        if (preg_match("/^([A-Za-zА-Яа-я])+/", $input_line, $matches, PREG_OFFSET_CAPTURE) === FALSE) {
            return [];
        }
        return true;
    }

    /**
     * Are 2 values equal?
     * 
     * @param mixed $value1
     * @param mixed $value2
     * 
     * @return boolean
     */
    public static function equalTo($value1, $value2) {
        if ($value1 !== $value2) {
            return [];
        }
        return true;
    }

    /**
     * Validates captcha
     * 
     * @param string $input Captcha input
     * @return boolean
     */
    public static function captcha($input) {
        if (!Captcha::validate($input)) {
            return [];
        }
        return true;
    }
    
    /**
     * Date validation
     * @param string $input Date input
     * @return boolean
     */
    public static function date($input) {
        $parse = explode('.', $input, 3);
        if(!preg_match('/^\d{2}.\d{2}.\d{4}$/', $input) || !checkdate($parse[1], $parse[0], $parse[2])) {
            return [];
        }
        return false;
    }
    
    /**
     * Time validation
     * @param string $input Date input
     * @return boolean
     */
    public static function time($input) {
        if(!preg_match('/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/', $input)) {
            return [];
        }
        return false;
    }

}
