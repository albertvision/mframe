<?php

namespace MFrame\Libraries;

/**
 * File System class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2014 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core/Libraries
 */
class FileSystem {
    
    /**
     * Recursive dir removing
     * 
     * @param string $dir Directory location
     * @return boolean
     */
    public static function rmDir($dir) {
        if(is_dir($dir) && is_writable($dir)) {
            $handle = opendir($dir);

            while (false !== ($entry = readdir($handle))) {
                $entryLocation = $dir . '/' . $entry;
                if ($entry != '.' && $entry != '..') {
                    if(is_dir($entryLocation)) {
                        if(!self::rmDir($entryLocation)) {
                            return false;
                        }
                    } else {
                        if(!self::unlink($entryLocation)) {
                            return false;
                        }
                    }
                }
            }
            closedir($handle);
            return @rmdir($dir);
        }
        return false;
    }
    
    /**
     * Removes a file
     * 
     * @param string $file File location
     * @return boolean
     */
    public static function unlink($file) {
        if(!is_dir($file) && is_writable($file)) {
            return @unlink($file);
        }
        return false;
    }
    
    /**
     * Appends content in a file
     * 
     * @param string $file File location
     * @param string $content Content to append
     * 
     * @return boolean
     */
    public static function fileAppend($file, $content) {
        if(is_file($file) && is_writable($file)) {
            return @file_put_contents($file, $content, FILE_APPEND);
        }
        return false;
    }
}
