<?php

namespace MFrame\Libraries;

/**
 * Database class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2014 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core/Libraries
 */
class DB {

    private static $_defaultConnection = 'default';
    private static $_publicConnection = NULL;
    private static $_instances = [];
    private static $_pdoObjects = [];
    private static $_config = [];
    
    /**
     * Current PDO object
     * 
     * @var object 
     * @return \PDO
     */
    private $_pdo = null;
    private $_stmt, $_params;

    public function __construct($con) {
        if (!self::$_config) {
            self::$_config = \MFrame\Config::get('database');
        }

        if ($con instanceof \PDO) {
            $pdoObj = $con;
            $con = md5(json_encode($con));
        } elseif(is_array($con)) {
            $pdoData = $con;
            $con = md5(json_encode($con));
        } elseif (is_string($con) && self::$_config[$con]) {
            $pdoData = self::$_config[$con];
        } else {
            throw new \Exception('Invalid database connection [' . $con . '].', 500);
        }
        
        if($pdoData && !$pdoObj) {
            $pdoObj = new \PDO((isset($pdoData['driver']) ? $pdoData['driver'] : 'mysql') . ':host=' . $pdoData['dbhost'] . ';dbname=' . $pdoData['dbname'], $pdoData['dbuser'], $pdoData['dbpass'], (isset($pdoData['pdo_options']) ? $pdoData['pdo_options'] : array()));
        }
        
        self::$_pdoObjects[$con] = $pdoObj;
        $this->_pdo = $pdoObj;
    }

    public static function connect($con, $global = true) {
        if (!$con) {
            $con = self::$_defaultConnection;
        }

        if ($con instanceof \PDO || is_array($con)) {
            $pdo_obj = $con;
            $con = md5(json_encode($con));
        }

        if (!self::$_instances[$con]) {
            self::$_instances[$con] = new DB($pdo_obj ? $pdo_obj : $con);
        }
        if ($global) {
            self::$_publicConnection = $con;
        }

        return self::$_instances[$con];
    }

    /**
     * Create a prepare statement
     * 
     * @param string $query Query
     * @param string $params Parameters
     * @param array $pdoOptions PDO options
     * 
     * @return \MFrame\Libraries\DB
     */
    private function _prepare($query, $params = [], $pdoOptions = []) {
        $this->_stmt = $this->_pdo->prepare($query, $pdoOptions);
        $this->_params = $params;
        
        return $this;
    }
    
    /**
     * Execute query
     * 
     * @param array $params Query's parameters if they are not defined in prepare() method
     * @return \MFrame\Libraries\DB
     */
    private function _execute($params = []) {
        if (count($params)) {
            $this->_params = $params;
        }
        
        $this->_stmt->execute($this->_params);
        if($this->_getError() !== FALSE) {
            if(\MFrame\Config::get('main.debug') == TRUE) {
                throw new \Exception(self::getError(), 500);
            } else {
                //@TODO
                return false;
            }
        }
        return $this;
    }
    
    
    /**
     * Create auto executing query
     * 
     * @param string $query Query
     * @param array $params Query's parameters
     * @param array $pdoOptions PDO options
     * 
     * @return \MFrame\Libraries\DB
     */
    private function _query($query, $params = [], $pdoOptions = []) {
        return $this->_prepare($query, $params, $pdoOptions)->execute();
    }
    
    /**
     * Fetching of query
     * 
     * @return array 
     */
    private function _fetchAssoc() {
        return $this->_stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    /**
     * Get a row
     */
    private function _fetchRowAssoc() {
        return $this->_stmt->fetch(\PDO::FETCH_ASSOC);
    }
    
    /**
     * Fetching of column
     * 
     * @param string $column Column name
     * @return array
     */
    private function _fetchColumn($column) {
        return $this->_stmt->fetchAll(\PDO::FETCH_COLUMN, $column);
    }
    
    /**
     * Get ID of the last inserted row
     * 
     * @return int
     */
    private function _lastID() {
        return $this->_pdo->lastInsertId();
    }
    
    /**
     * Get count of the affected rows
     * 
     * @return int
     */
    private function _numRows() {
        return $this->_stmt->rowCount();
    }
    
    /**
     * Get prepare statement's property. For advanced users!
     * 
     * @return object
     */
    private function _getSTMT() {
        return $this->_stmt;
    }
    
    /**
     * Get SQL Error Code
     * 
     * @return string|bool
     */
    private function _getError() {
        $err = $this->_stmt->errorCode();
        $errInfo = $this->_stmt->errorInfo();
        
        if($err != '00000') {
            return $errInfo[2];
        }
        return false;
    }
    
    /**
     * Return the active PDO object
     * @return object
     */
    private function _getPdo() {
        return self::$_pdoObjects[self::$_publicConnection];
    }

    /**
     * 
     * @param string $name Method name
     * @param array $attr Attributes
     * 
     * @return \MFrame\Libraries\DB
     * @throws \Exception
     */
    public static function __callStatic($name, $attr) {
        $instance = self::connect(self::$_publicConnection);
        $method = "_$name";

        if (is_callable(array($instance, $method))) {
            return call_user_func_array(array($instance, $method), $attr);
        } else {
            throw new \Exception('DB method [' . $name . '] not found.', 500);
        }
    }

    /**
     * 
     * @param string $name Method name
     * @param array $attr Attributes
     * 
     * @return \MFrame\Libraries\DB
     * @throws \Exception
     */
    public function __call($name, $attr) {
        $method = "_$name";

        if (is_callable(array($this, $method))) {
            return call_user_func_array(array($this, $method), $attr);
        } else {
            throw new \Exception('DB method [' . $name . '] not found.', 500);
        }
    }

}
