<?php

namespace MFrame\Libraries;

/**
 * Captcha class. 
 * Based on http://www.sitepoint.com/simple-captchas-php-gd.
 *
 * @author Mehul Jain
 * @link http://www.sitepoint.com/simple-captchas-php-gd
 * @package Core/Libraries
 */
class Captcha {

    /**
     * Image content
     * @var string
     */
    private $_image;

    /**
     * Captcha content
     * @var string
     */
    private $_text;

    /**
     * Created an captcha image
     * 
     * @param int $width Captcha width
     * @param int $height Captcha height
     * @param int $lines Lines in the captcha
     * @param int $bgDots Background dots in the captcha
     */
    public function __construct($width, $height, $lines, $bgDots) {
        $text = self::getSess();
        if (!$text) {
            $text = self::setSess();
        }

        $this->_image = imagecreatetruecolor($width, $height);
        $textColor = imagecolorallocate($this->_image, 0, 0, 0);

        $background_color = imagecolorallocate($this->_image, 255, 255, 255);
        imagefilledrectangle($this->_image, 0, 0, $width, $height, $background_color);

        $line_color = imagecolorallocate($this->_image, 64, 64, 64);

        for ($i = 0; $i < $lines; $i++) {
            imageline($this->_image, 0, rand() % $height, $width, rand() % $height, $line_color);
        }

        $pixel_color = imagecolorallocate($this->_image, 0, 0, 255);
        for ($i = 0; $i < $bgDots; $i++) {
            imagesetpixel($this->_image, rand() % $width, rand() % $height, $pixel_color);
        }

        $lettersWidth = strlen($text) * 9;
        imagestring($this->_image, 5, ($width / 2) - $lettersWidth / 2, ($height / 2) - 7, $text, $textColor);
    }

    /**
     * Displays the captcha
     */
    public function output() {
        header('Content-type: image/png');
        imagepng($this->_image);
    }

    /**
     * Returns current image in string
     * 
     * @return string
     */
    public function getOutput() {
        return $this->_image;
    }

    /**
     * Returns captcha's content
     * 
     * @return string
     */
    public function getContent() {
        return $this->_text;
    }

    /**
     * Creates an instance of Captcha() class 
     * 
     * @param int $width Captcha width
     * @param int $height Captcha height
     * @param int $lines Lines in the captcha
     * @param int $bgDots Background dots in the captcha
     * @return \MFrame\Libraries\Captcha
     */
    public static function generate($width = 150, $height = 50, $lines = 3, $bgDots = 1000) {
        return new Captcha($width, $height, $text, $lines, $bgDots);
    }

    /**
     * Validates the provided text with the captcha's text
     * 
     * @param string $value Input text
     * @return boolean
     */
    public static function validate($value) {
        $sess = self::getSess();
        \MFrame\Sessions\Session::remove('uc_captcha');

        if ($value !== NULL && strlen($value) > 0 && $value === $sess) {
            return true;
        }
        return false;
    }

    /**
     * Puts a random text into session
     * 
     * @param string $text Captcha's text
     * @return string Generated text
     */
    public static function setSess($text = null) {
        if (!$text) {
            $text = self::genText();
        }
        \MFrame\Sessions\Session::set('uc_captcha', $text);

        return $text;
    }

    /**
     * Returns captcha's value which is located in the session
     * @return string
     */
    public static function getSess() {
        return \MFrame\Sessions\Session::get('uc_captcha');
    }

    /**
     * Generates a captcha's text
     * 
     * @param string $length Length of the captcha's text
     * @return string Generated text
     */
    public static function genText($length = 6) {
        return substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'), 0, $length);
    }

}
