<?php

namespace MFrame\Libraries;

/**
 * Data hashing class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core/Libraries
 */
class Hash  {
    
    /**
     * Data hashing based to PHP password_hash() function.
     * 
     * @param   string  $string     String to hashing
     * @param   string  $algorithm  Hashing algorithm
     * @param   array   $options    Options
     * 
     * @return string Hashed data
     */
    public static function make($string, $algorithm = null, $options = []) {
        $_config = \MFrame\Config::get('hashing');
        
        if(!$algorithm) {
            $algorithm = $_config['algorithm'];
        }
        
        $options = array_merge($_config['options'], $options);
        return password_hash($string, $algorithm, $options);
    }
    
    /**
     * Validate password hash
     * @param   string  $string     String
     * @param   string  $hash       Hashed string
     * 
     * @return boolean
     */
    public static function check($string, $hash) {
        return password_verify($string, $hash);
    }
    
    /**
     * Password needs rehash?
     * 
     * @param   string    $hash         Hash string
     * @param   int|null  $algorithm    Hashing algorithm
     * @param   array     $options      Options
     * 
     * @return  boolean
     */
    public static function needsRehash($hash, $algorithm = null, $options = []) {
        $_config = \MFrame\Config::get('hashing');
        
        if(!$algorithm) {
            $algorithm = $_config['algorithm'];
        }
        $options = array_merge($_config['options'], $options);
        return password_needs_rehash($hash, $algorithm, $options);
    }    
}
?>