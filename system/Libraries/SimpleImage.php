<?php

/**
 * SimpleImage class
 *
 * @author Simon Jarvis
 * @link http://framework.maleeby.com/ <http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php>
 * @copyright Copyright &copy; 2006 Simon Jarvis
 * @license http://www.gnu.org/licenses/gpl.html
 * @package Core.Libraries
 */

namespace MFrame\Libraries;

class SimpleImage {

    private $_image;
    private $_imageType;

    public function __construct($filename) {
        $image_info = getimagesize($filename);
        $this->_imageType = $image_info[2];
        if ($this->_imageType == IMAGETYPE_JPEG) {
            $this->_image = imagecreatefromjpeg($filename);
        } elseif ($this->_imageType == IMAGETYPE_GIF) {
            $this->_image = imagecreatefromgif($filename);
        } elseif ($this->_imageType == IMAGETYPE_PNG) {
            $this->_image = imagecreatefrompng($filename);
        }
    }

    /**
     * Makes instance of this class
     * 
     * @param string $filename Image location
     * 
     * * @return \MFrame\Libraries\SimpleImage
     */
    public static function load($filename) {
        return new SimpleImage($filename);
    }

    /**
     * Saves an image
     * 
     * @param string $filename New file name
     * @param int $image_type Image type
     * @param int $compression Image compression
     * @param int $permissions Permissions to save
     */
    public function save($filename, $compression = 75, $permissions = null) {
        $filename = sprintf($filename, $this->getWidth(), $this->getHeight());

        if ($this->_imageType == IMAGETYPE_JPEG) {
            $return = imagejpeg($this->_image, $filename, $compression);
        } elseif ($this->_imageType == IMAGETYPE_GIF) {
            $return = imagegif($this->_image, $filename);
        } elseif ($this->_imageType == IMAGETYPE_PNG) {
            $return = imagepng($this->_image, $filename);
        } if ($permissions != null) {
            chmod($filename, $permissions);
        }

        return $return ? $filename : $return;
    }
    
    /**
     * Destroys the image
     * 
     * @return bool
     */
    public function destroy() {
        return imagedestroy($this->_image);
    }

    /**
     * Get the output of an image
     * 
     * @param int $image_type
     */
    public function output($image_type = IMAGETYPE_JPEG) {
        if ($image_type == IMAGETYPE_JPEG) {
            imagejpeg($this->_image);
        } elseif ($image_type == IMAGETYPE_GIF) {
            imagegif($this->_image);
        } elseif ($image_type == IMAGETYPE_PNG) {
            imagepng($this->_image);
        }
    }

    /**
     * Get image's width
     * 
     * @return int Image's width
     */
    public function getWidth() {
        return imagesx($this->_image);
    }

    /**
     * Get image's height
     * 
     * @return int Image's height
     */
    public function getHeight() {
        return imagesy($this->_image);
    }

    /**
     * Resizes image height. Saves proportions
     * 
     * @param int $height New height
     * @return \MFrame\Libraries\SimpleImage
     */
    public function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width, $height);

        return $this;
    }

    /**
     * Resizes image width. Saves proportions
     * 
     * @param int $width New width
     * @return \MFrame\Libraries\SimpleImage
     */
    public function resizeToWidth($width) {
        $originalWidth = $this->getWidth();
        if ($originalWidth < $width) {
            $width = $originalWidth;
        }

        $ratio = $width / $originalWidth;
        $height = $this->getHeight() * $ratio;
        $this->resize($width, $height);

        return $this;
    }

    /**
     * Resize in percents
     * 
     * @param int $scale Percents to resize
     * @return \MFrame\Libraries\SimpleImage
     */
    public function scale($scale) {
        $width = $this->getWidth() * $scale / 100;
        $height = $this->getheight() * $scale / 100;
        $this->resize($width, $height);

        return $this;
    }

    /**
     * Resize in pixels
     * 
     * @param int $maxWidth New width
     * @param int $maxHeight New height
     * 
     * @return \MFrame\Libraries\SimpleImage
     */
    public function resize($maxWidth, $maxHeight, $saveRatio = true) {
        if($saveRatio) {
            $size = $this->getResizedSize($maxWidth, $maxHeight);
            $maxWidth = $size['width'];
            $maxHeight = $size['height'];
        }
        
        $new_image = imagecreatetruecolor($maxWidth, $maxHeight);
        imagecopyresampled($new_image, $this->_image, 0, 0, 0, 0, $maxWidth, $maxHeight, $this->getWidth(), $this->getHeight());
        $this->_image = $new_image;

        return $this;
    }
    
    /**
     * Create a thumb of image
     * 
     * @param int $maxWidth Maximum width
     * @param int $maxHeight Maximum height
     * 
     * @return \MFrame\Libraries\SimpleImage
     */
    public function createThumb($maxWidth, $maxHeight) {
        $originalSize = [
            'width' => $this->getWidth(),
            'height' => $this->getHeight()
        ];
        
        if($maxWidth > $originalSize['width']) {
            $maxWidth = $originalSize['width'];
        }
        if($maxHeight > $originalSize['height']) {
            $maxHeight = $originalSize['height'];
        }
        
        $size = $this->getResizedSize($maxWidth, $maxHeight);
        
        if($size['width'] > $size['height']) {
            $this->resizeToHeight($maxHeight);
        } elseif($size['width'] < $size['height']) {
            $this->resizeToWidth($maxWidth);
        }
        
        $this->cropCentered($maxWidth, $maxHeight);
        return $this;
    }

    /**
     * Get new size by passing the maximum
     * 
     * @param int $maxWidth Maximum width
     * @param int $maxHeight Maximum height
     * 
     * @return array New size
     */
    public function getResizedSize($maxWidth, $maxHeight) {
        $size = [
            'width' => $this->getWidth(),
            'height' => $this->getHeight()
        ];
        $resize = [
            'width' => $maxWidth,
            'height' => $maxHeight
        ];

        if ($size['width'] < $maxWidth) {
            $resize['width'] = $size['width'];
        }
        if ($size['height'] < $maxHeight) {
            $resize['height'] = $size['height'];
        }

        $ratio = [
            'width' => $resize['width'] / $size['width'],
            'height' => $resize['height'] / $size['height']
        ];

        if ($size['width'] > $size['height']) {
            $resize = [
                'width' => $resize['width'],
                'height' => $size['height'] * $ratio['width'],
            ];
        } elseif($size['width'] < $size['height']) {
            $resize = [
                'width' => $size['width'] * $ratio['height'],
                'height' => $resize['height'],
            ];
        }
        
        return $resize;
    }

    /**
     * Crop centraly an image
     * 
     * @param int $width Cut image width
     * @param int $height Cut image height
     * 
     * @return \MFrame\Libraries\SimpleImage
     */
    public function cropCentered($width, $height) {
        $cx = $this->getWidth() / 2;
        $cy = $this->getHeight() / 2;

        $x = $cx - $width / 2;
        $y = $cy - $height / 2;
        
        if ($x < 0) {
            $x = 0;
        }
        if ($y < 0) {
            $y = 0;
        }

        $this->_image = imagecrop($this->_image, [
            'x' => $x,
            'y' => $y,
            'width' => $width,
            'height' => $height
        ]);

        return $this;
    }

}

?>