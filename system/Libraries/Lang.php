<?php

namespace MFrame\Libraries;

/**
 * Translation class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core/Libraries
 */
class Lang {

    /**
     * Current language
     * @var string
     */
    private static $_lang = null;
    /**
     * Language data
     * @var array
     */
    private static $_data = [];

    /**
     * Set current language
     * 
     * @param string|null $lang Language abbreviation
     * @return boolean
     * @throws \Exception
     */
    public static function setLang($lang = NULL) {
        if (!$lang) {
            $lang = \MFrame\Config::get('main.default_lang');
        }

        $path = \MFrame\Loader::fixPath(APP_PATH . '/languages/' . $lang);
        if (!file_exists($path) || !is_dir($path) || !is_readable($path)) {
            throw new \Exception('Language folder [' . $path . '] cannot be used.', 500);
        }
        self::$_lang = $lang;
        return true;
    }
    
    /**
     * Get current language
     * @return string
     */
    public static function getLang() {
        if (!self::$_lang) {
            self::setLang();
        }
        
        return self::$_lang;
    }

    /**
     * Get language string
     * 
     * @param string $notation Notation
     * @param string|null $default Default value
     * 
     * @return mixed
     * @throws \Exception
     */
    public static function get($notation, $default = null) {
        if (!self::$_lang) {
            self::setLang();
        }

        $exploded = explode('.', $notation, 2);

        if (!self::$_data[$exploded[0]]) {
            $filename = \MFrame\Loader::fixPath(APP_PATH . '/languages/' . self::$_lang . '/' . $exploded[0] . '.php');
            if (file_exists($filename) && is_readable($filename) && is_file($filename)) {
                $data = include $filename;

                if (is_array($data)) {
                    self::$_data[$exploded[0]] = $data;
                } else {
                    throw new \Exception('Invalid language file [' . $filename . '].', 500);
                }
            } else {
                throw new \Exception('Language file [' . $filename . '] not found.', 500);
            }
        }

        $result = array_get($exploded[1], self::$_data[$exploded[0]]);

        return $result ? $result : $default;
    }
    
    /**
     * Puralization of string
     * @param string $notation String notation
     * @param int $num Number
     * 
     * @return mixed
     */
    public static function choise($notation, $num = 0) {
        if (!self::$_lang) {
            self::setLang();
        }
        
        return string_pluralization(self::get($notation), $num);
    }

    /**
     * String exists?
     * 
     * @param string $notation String notation
     * @return bool
     */
    public static function has($notation) {
        if (!self::$_lang) {
            self::setLang();
        }
        
        return (bool) self::get($notation);
    }
    
    /**
     * Translates date format
     * 
     * @param int $timestamp Timestamp
     * @return string Translated date format
     */
    public static function date($format, $timestamp = NULL) {
        if (!self::$_lang) {
            self::setLang();
        }
        
        $en['months'] = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $en['weeks'] = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');

        if(self::has('system.date')) {
            $lang = self::get('system.date');
        } else {
            $lang = $en;
        }
        
        $format = str_ireplace($en['months'], $lang['months'], date($format, ($timestamp == NULL ? time() : $timestamp)));
        $format = str_replace($en['weeks'], $lang['weeks'], $format);
        
        return $format;
    }

}
