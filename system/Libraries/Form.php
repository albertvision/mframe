<?php

namespace MFrame\Libraries;

class Form {

    public static function select($name, $values = [], $default = NULL, $customHTMLParameters = '') {
        $return = '<select name="' . $name . '" ' . $customHTMLParameters . '>';
        foreach ($values as $key => $value) {
            $return .= '<option value="'.$key.'" '.($key == $default ? 'selected' : null).'>'.$value.'</option>';
        }
        $return .= '</select>';
        
        return $return;
    }

}
