<?php

namespace MFrame;

/**
 * AutoLoader class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core
 */
class AutoLoader {

    /**
     * Is Autoloader class initialized
     * @var bool 
     */
    private static $_init = false;

    /**
     * Registered namespaces
     * @var array
     */
    private static $_namespaces = array();

    private function __construct() {
        ;
    }

    /**
     * Initialize AutoLoader
     * @return bool
     */
    public static function init() {
        if (!self::$_init) {
            spl_autoload_register(__NAMESPACE__ . '\AutoLoader::registerAutoLoad');
            self::$_init = true;
        }
        return self::$_init;
    }

    /**
     * AutoLoader method
     * @param string $class Method to load
     * @throws \Exception
     */
    public static function registerAutoLoad($class) {
        foreach (self::$_namespaces as $namespace => $path) {
            if (strpos($class, $namespace) === 0) {
                $_filepath = Core::fixPath($path . DIRECTORY_SEPARATOR . substr($class, strlen($namespace)) . '.php');

                if (file_exists($_filepath)) {
                    include $_filepath;
                } else {
                    throw new \Exception('File [' . $_filepath . '] not found', 404);
                }
                break;
            }
        }
    }

    /**
     * Register namespace
     * @param string $namespaceName Namespace name
     * @param string $namespacePath Namespace path
     * @throws \Exception
     */
    public static function setNamespace($namespaceName, $namespacePath) {
        if (is_readable($namespacePath) && is_dir($namespacePath)) {
            if ($namespaceName[0] == '\\') {
                $namespaceName = substr($namespaceName, 1);
            }
            if ($namespaceName[strlen($namespaceName)] != '\\') {
                $namespaceName .= '\\';
            }
            self::$_namespaces[$namespaceName] = realpath($namespacePath);
        } else {
            throw new \Exception('Invalid namespace path [' . $namespacePath . ']', 500);
        }
    }

}
