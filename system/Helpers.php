<?php

/**
 * Rederecting
 * 
 * @param string $url Redirect URL
 * @param boolean $fullURL Full URL?
 */
function redirect($url, $fullURL = false) {
    header('Location: ' . ($fullURL ? '' : BASE_URL . '/') . $url);
    die();
}

/**
 * String generating
 * 
 * @param int $length String length
 * @return string Generated string
 */
function generate_string($length) {
    $random = str_shuffle('0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM');
    $cut = substr($random, 0, $length);
    return $cut;
}

/**
 * Transliterating of string
 * 
 * @static
 * @deprecated since version 2.0
 * @param string $text String to transliterate
 * @return string Transliterated string
 */
function transliterate2bg($text) {
    $en = array("a", "b", "v", "g", "d", "e", "zh", "z", "i", "i", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "sht", "u", "io", "iu", "q", "a", "b", "v", "g", "d", "e", "zh", "z", "i", "ii", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "sht", "u", "io", "iu", "q", "ch", "sh", "sht", "_", "", "", "", "", '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''); //латинските букви
    $bg = array("а", "б", "в", "г", "д", "е", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ь", "ю", "я", "А", "Б", "В", "Г", "Д", "Е", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ь", "Ю", "Я", "ч", "ш", "щ", " ", ",", ".", "&", "'", '"', "!", "?", "(", "[", "]", ")", ";", ":", "-", "”", "„", "+", "“", "/"); //кирилица, съответстващ на латиницата по-горе
    
    $transform = str_replace($bg, $en, trim($text));
    $ready = str_replace("__", "_", trim($transform));

    return $ready;
}

/**
 * Transliterates a Cyrillic string to latin.
 * 
 * @param string $text Cyriilic string
 * @return string Latin string
 */
function cyrillic2latin($text) {
    $en = array("a", "b", "v", "g", "d", "e", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "sht", "u", "yo", "yu", "ya", "A", "B", "V", "G", "D", "E", "Zh", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "Tc", "Ch", "Sh", "Sht", "U", "Yo", "Yu", "Ya"); //латинските букви
    $bg = array("а", "б", "в", "г", "д", "е", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ь", "ю", "я", "А", "Б", "В", "Г", "Д", "Е", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ь", "Ю", "Я"); //кирилица, съответстващ на латиницата по-горе

    return str_replace($bg, $en, trim($text));
}

/**
 * Makes a slug from title. Replaces special symbols like ")(^%$" with "-"
 * 
 * @param string $title Title
 * @return string
 */
function makeSlug($title) {
    $latin = cyrillic2latin($title);
    $forbiddenSymbols = [' ','`','~','!','@','#','$','%','^','&','*','(',')','_','+','-','=','[',']','\\',';','\'',',','.','/','{','}','|',':','"','<','>','?','€'];
    $replacement = '-';
    
    /*
     * Replaces the forbidden symbols with "-" and after that removes the repeatable "-".
     */
    $slug = preg_replace('#'.$replacement.'+#', $replacement, str_replace($forbiddenSymbols, $replacement, mb_strtolower($latin,'utf8')));
    
    /*
     * If the slug ends on "-", removes the "-".
     */
    return substr($slug, -1) == $replacement ? substr($slug, 0, strlen($slug)-1) : $slug;
}

/**
 * Removes folder and content in it
 * @static
 * @param string $dir Folder path
 */
function remove_dir($dir) {
    if (is_dir($dir)) {
        $obj = scandir($dir);
        foreach ($obj as $object) {
            if ($object != '.' && $object != '..') {
                if (is_dir($dir . '/' . $object)) {
                    remove_dir($dir . '/' . $object);
                } else {
                    unlink($dir . '/' . $object);
                }
            }
        }
        if (rmdir($dir)) {
            return true;
        }
    }
    return false;
}

/**
 * XML to Array convertion
 * @param string $xml XML to convert
 * @return string
 */
function xml2array($xml) {
    $xml = simplexml_load_file($xml);
    $xml_array = unserialize(serialize(json_decode(json_encode((array) $xml), 1)));

    return $xml_array;
}

/**
 * Cleans the slashes in URL
 * @param string $url URL to clean
 * @return string
 */
function clearURL($url) {
    $url = preg_replace('#/+#', '/', $url);

    if ($url[0] == '/') {
        $url = substr($url, 1, strlen($url));
    } if (substr($url, -1) == '/') {
        $url = substr($url, 0, strlen($url) - 1);
    }
    
    return $url;
}

/**
 * Deep search in array
 * @param array|string $needle What to search
 * @param array $haystack Array to search in it
 * @return boolean
 */
function array_deep_search($needle, $haystack) {
    if (in_array($needle, $haystack)) {
        return true;
    }
    foreach ($haystack as $element) {
        if (is_array($element) && array_deep_search($needle, $element)) {
            return true;
        }
    }
    return false;
}

/**
 * Get array value by point notation
 * 
 * @param string $notation Notation
 * @param array $array Array
 * @param mixed $default Default value
 * 
 * @return mixed
 */
function array_get($notation, $array, $default = null) {
    if (is_string($notation)) {
        $notation = explode('.', $notation);
    } elseif (!$notation) {
        return $array;
    }

    if (is_array($notation)) {
        $key = array_shift($notation);

        $res = $array[$key];
        if (is_array($array[$key]) && count($notation)) {
            $res = array_get($notation, $array[$key]);
        }
    }

    return ($res !== NULL ? $res : $default);
}

/**
 * Set value of array notation
 * 
 * @param string $notation Notation
 * @param mixed $value New value
 * @param array $array Array
 * 
 * @return array New array
 */
function array_set($notation, $value, $array) {
    if (is_string($notation)) {
        $notation = explode('.', $notation);
    }
    if (is_array($notation)) {
        $key = array_shift($notation);
        if (count($notation) && !is_array($array[$key])) {
            $array[$key] = [];
        }
        if (!count($notation) || !is_array($array[$key])) {

            if ($value === NULL) {
                unset($array[$key]);
            } else {
                $array[$key] = $value;
            }
        } else {
            $array[$key] = array_set($notation, $value, $array[$key]);
        }
    }
    return $array;
}

/**
 * Inserts values before specific key.
 *
 * @param array $array
 * @param sting/integer $position
 * @param array $values
 * @throws Exception
 */
function array_before(array &$array, $position, array $values) {
    // enforce existing position
    if (!isset($array[$position])) {
        throw new \Exception(strtr('Array position does not exist (:1)', [':1' => $position]));
    }

    // offset
    $offset = -1;

    // loop through array
    foreach ($array as $key => $value) {
        // increase offset
        ++$offset;

        // break if key has been found
        if ($key == $position) {
            break;
        }
    }

    $array = array_slice($array, 0, $offset, TRUE) + $values + array_slice($array, $offset, NULL, TRUE);

    return $array;
}

/**
 * Inserts values after specific key.
 *
 * @param array $array
 * @param sting/integer $position
 * @param array $values
 * @throws Exception
 */
function array_after(array &$array, $position, array $values) {
    // enforce existing position
    if (!isset($array[$position])) {
        throw new \Exception(strtr('Array position does not exist (:1)', [':1' => $position]));
    }

    // offset
    $offset = 0;

    // loop through array
    foreach ($array as $key => $value) {
        // increase offset
        ++$offset;

        // break if key has been found
        if ($key == $position) {
            break;
        }
    }

    $array = array_slice($array, 0, $offset, TRUE) + $values + array_slice($array, $offset, NULL, TRUE);

    return $array;
}

/**
 * Get string between 2 strings
 * 
 * @param string $haystack Haystack
 * @param string $needle_start Start string
 * @param string $needle_end End string
 * 
 * @return string Needle
 */
function between2strings($haystack, $needle_start, $needle_end) {
    $startsAt = strpos($haystack, $needle_start) + strlen($needle_start);
    $endsAt = strpos($haystack, $needle_end, $startsAt);

    return substr($haystack, $startsAt, $endsAt - $startsAt);
}

/**
 * String pluralization
 * 
 * @param string $string String
 * @param int $num Number
 * 
 * @return string
 * @throws \Exception
 */
function string_pluralization($string, $num) {
    if (!is_string($string)) {
        throw new \Exception('Invalid string [' . $string . '].');
    }
    $parts = explode('|', $string);

    foreach ($parts as $i => $part) {
        $limit = between2strings($part, '[', ']');
        $min_max = explode(',', $limit, 2);
        $part = trim(mb_substr(strstr($part, ']'), 1));

        if (is_numeric($limit) && $num == $limit) {
            break;
        } elseif (!$min_max[0] && $num <= $min_max[1]) {
            break;
        } elseif ($num >= $min_max[0] && !$min_max[1] && count($min_max) == 2) {
            break;
        } elseif ($num >= $min_max[0] && $num <= $min_max[1]) {
            break;
        }
    }

    return str_replace('{num}', $num, $part);
}

/**
 * Check if a string is serialized
 * 
 * @param string $serializedString
 * @return bool
 */
function is_serialized($serializedString) {
    return (@unserialize($serializedString) !== false);
}

/**
 * Replaces string once
 * 
 * @param string $search Search string
 * @param string $replace Replace string
 * @param string $subject Haystack
 * 
 * @return string
 */
function str_replace_once($search, $replace, $subject) {
    $pos = strpos($subject, $search);
    if ($pos !== false) {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }
    return $subject;
}

/**
 * Shortens an URL
 * 
 * @param int $url URL
 * @param boolean $full TRUE to return the array result, FALSE to return just the shortened
 * 
 * @return array|string|boolean
 */
function shorten($url, $full = false) {
    $ch = curl_init();

    curl_setopt_array($ch, [
        CURLOPT_URL => 'https://www.googleapis.com/urlshortener/v1/url',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => '{ "longUrl": "' . $url . '" }',
        CURLOPT_HTTPHEADER => [
            'Content-type: application/json'
        ],
        CURLOPT_SSL_VERIFYPEER => false
    ]);


    $res = curl_exec($ch);
    curl_close($ch);

    if ($res) {
        $res = json_decode($res, true);

        return $full ? $res : $res['id'];
    }
    return false;
}

/**
 * Converts an array to XML
 * 
 * @param array $array Input array
 * @return string XML Output
 */
function xml_encode($array = []) {
    if (is_array($array)) {
        foreach ($array as $element) {
            if (!$element['tag'])
                continue;
            $return .= '<' . $element['tag'];

            if (is_array($element['attrs']) && count($element['attrs'])) {
                foreach ($element['attrs'] as $attrKey => $attrVal) {
                    $return .= ' ' . $attrKey . '="' . $attrVal . '"';
                }
            }

            $return .= '>';
            if (is_string($element['value'])) {
                $return .= $element['value'];
            } elseif (is_array($element['value'])) {
                $return .= xml_encode($element['value']);
            }
            $return .= '</' . $element['tag'] . '>';
        }
    }
    return $return;
}

/**
 * A Compatibility library with PHP 5.5's simplified password hashing API.
 *
 * @author Anthony Ferrara <ircmaxell@php.net>
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 * @copyright 2012 The Authors
 */
if (!defined('PASSWORD_DEFAULT')) {

    define('PASSWORD_BCRYPT', 1);
    define('PASSWORD_DEFAULT', PASSWORD_BCRYPT);

    /**
     * Hash the password using the specified algorithm
     *
     * @param string $password The password to hash
     * @param int    $algo     The algorithm to use (Defined by PASSWORD_* constants)
     * @param array  $options  The options for the algorithm to use
     *
     * @return string|false The hashed password, or false on error.
     */
    function password_hash($password, $algo, array $options = array()) {
        if (!function_exists('crypt')) {
            trigger_error("Crypt must be loaded for password_hash to function", E_USER_WARNING);
            return null;
        }
        if (!is_string($password)) {
            trigger_error("password_hash(): Password must be a string", E_USER_WARNING);
            return null;
        }
        if (!is_int($algo)) {
            trigger_error("password_hash() expects parameter 2 to be long, " . gettype($algo) . " given", E_USER_WARNING);
            return null;
        }
        switch ($algo) {
            case PASSWORD_BCRYPT:
                // Note that this is a C constant, but not exposed to PHP, so we don't define it here.
                $cost = 10;
                if (isset($options['cost'])) {
                    $cost = $options['cost'];
                    if ($cost < 4 || $cost > 31) {
                        trigger_error(sprintf("password_hash(): Invalid bcrypt cost parameter specified: %d", $cost), E_USER_WARNING);
                        return null;
                    }
                }
                // The length of salt to generate
                $raw_salt_len = 16;
                // The length required in the final serialization
                $required_salt_len = 22;
                $hash_format = sprintf("$2y$%02d$", $cost);
                break;
            default:
                trigger_error(sprintf("password_hash(): Unknown password hashing algorithm: %s", $algo), E_USER_WARNING);
                return null;
        }
        if (isset($options['salt'])) {
            switch (gettype($options['salt'])) {
                case 'NULL':
                case 'boolean':
                case 'integer':
                case 'double':
                case 'string':
                    $salt = (string) $options['salt'];
                    break;
                case 'object':
                    if (method_exists($options['salt'], '__tostring')) {
                        $salt = (string) $options['salt'];
                        break;
                    }
                case 'array':
                case 'resource':
                default:
                    trigger_error('password_hash(): Non-string salt parameter supplied', E_USER_WARNING);
                    return null;
            }
            if (strlen($salt) < $required_salt_len) {
                trigger_error(sprintf("password_hash(): Provided salt is too short: %d expecting %d", strlen($salt), $required_salt_len), E_USER_WARNING);
                return null;
            } elseif (0 == preg_match('#^[a-zA-Z0-9./]+$#D', $salt)) {
                $salt = str_replace('+', '.', base64_encode($salt));
            }
        } else {
            $buffer = '';
            $buffer_valid = false;
            if (function_exists('mcrypt_create_iv') && !defined('PHALANGER')) {
                $buffer = mcrypt_create_iv($raw_salt_len, MCRYPT_DEV_URANDOM);
                if ($buffer) {
                    $buffer_valid = true;
                }
            }
            if (!$buffer_valid && function_exists('openssl_random_pseudo_bytes')) {
                $buffer = openssl_random_pseudo_bytes($raw_salt_len);
                if ($buffer) {
                    $buffer_valid = true;
                }
            }
            if (!$buffer_valid && is_readable('/dev/urandom')) {
                $f = fopen('/dev/urandom', 'r');
                $read = strlen($buffer);
                while ($read < $raw_salt_len) {
                    $buffer .= fread($f, $raw_salt_len - $read);
                    $read = strlen($buffer);
                }
                fclose($f);
                if ($read >= $raw_salt_len) {
                    $buffer_valid = true;
                }
            }
            if (!$buffer_valid || strlen($buffer) < $raw_salt_len) {
                $bl = strlen($buffer);
                for ($i = 0; $i < $raw_salt_len; $i++) {
                    if ($i < $bl) {
                        $buffer[$i] = $buffer[$i] ^ chr(mt_rand(0, 255));
                    } else {
                        $buffer .= chr(mt_rand(0, 255));
                    }
                }
            }
            $salt = str_replace('+', '.', base64_encode($buffer));
        }
        $salt = substr($salt, 0, $required_salt_len);

        $hash = $hash_format . $salt;

        $ret = crypt($password, $hash);

        if (!is_string($ret) || strlen($ret) <= 13) {
            return false;
        }

        return $ret;
    }

    /**
     * Get information about the password hash. Returns an array of the information
     * that was used to generate the password hash.
     *
     * array(
     *    'algo' => 1,
     *    'algoName' => 'bcrypt',
     *    'options' => array(
     *        'cost' => 10,
     *    ),
     * )
     *
     * @param string $hash The password hash to extract info from
     *
     * @return array The array of information about the hash.
     */
    function password_get_info($hash) {
        $return = array(
            'algo' => 0,
            'algoName' => 'unknown',
            'options' => array(),
        );
        if (substr($hash, 0, 4) == '$2y$' && strlen($hash) == 60) {
            $return['algo'] = PASSWORD_BCRYPT;
            $return['algoName'] = 'bcrypt';
            list($cost) = sscanf($hash, "$2y$%d$");
            $return['options']['cost'] = $cost;
        }
        return $return;
    }

    /**
     * Determine if the password hash needs to be rehashed according to the options provided
     *
     * If the answer is true, after validating the password using password_verify, rehash it.
     *
     * @param string $hash    The hash to test
     * @param int    $algo    The algorithm used for new password hashes
     * @param array  $options The options array passed to password_hash
     *
     * @return boolean True if the password needs to be rehashed.
     */
    function password_needs_rehash($hash, $algo, array $options = array()) {
        $info = password_get_info($hash);
        if ($info['algo'] != $algo) {
            return true;
        }
        switch ($algo) {
            case PASSWORD_BCRYPT:
                $cost = isset($options['cost']) ? $options['cost'] : 10;
                if ($cost != $info['options']['cost']) {
                    return true;
                }
                break;
        }
        return false;
    }

    /**
     * Verify a password against a hash using a timing attack resistant approach
     *
     * @param string $password The password to verify
     * @param string $hash     The hash to verify against
     *
     * @return boolean If the password matches the hash
     */
    function password_verify($password, $hash) {
        if (!function_exists('crypt')) {
            trigger_error("Crypt must be loaded for password_verify to function", E_USER_WARNING);
            return false;
        }
        $ret = crypt($password, $hash);
        if (!is_string($ret) || strlen($ret) != strlen($hash) || strlen($ret) <= 13) {
            return false;
        }

        $status = 0;
        for ($i = 0; $i < strlen($ret); $i++) {
            $status |= (ord($ret[$i]) ^ ord($hash[$i]));
        }

        return $status === 0;
    }

}