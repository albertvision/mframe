<?php

namespace MFrame;

/**
 * Configuration class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core
 */
class Config {

    /**
     * Loaded configuration data
     * @var array
     */
    private static $_configData = [];

    private function __construct() {
        ;
    }

    /**
     * Get config data
     * 
     * @param string $notation Config notation
     * @param mixed $default Default value
     * 
     * @return mixed
     */
    public static function get($notation = null, $default = null) {
        $data = explode('.', $notation, 2);
        if (!isset(self::$_configData[$data[0]]) && $notation) {
            self::load($data[0]);
        }

        return array_get($notation, self::$_configData, $default);
    }

    /**
     * Set configuration data
     * @param string $notation Notation
     * @param mixed $value New value
     * 
     * @return boolean
     */
    public static function set($notation, $value) {
        $data = explode('.', $notation, 2);
        if (!isset(self::$_configData[$data[0]])) {
            self::load($data[0]);
        }
        
        self::$_configData = array_set($notation, $value, self::$_configData);
        return true;
    }

    /**
     * Load configuration file
     * @param string $filename Configuration filename
     * @return boolean
     * @throws \Exception
     */
    public static function load($filename) {
        /*
         * If $filename is array
         */
        if(is_array($filename)) {
            foreach($filename as $configName) {
                self::load($configName);
            }
            return true;
        }
        $filename = Loader::fixPath(APP_PATH . '/config/' . $filename . '.php');

        if (is_file($filename) && is_readable($filename)) {
            $configData = include $filename;
            
            self::$_configData[basename($filename, '.php')] = is_array($configData) ? $configData : array();
            return true;
        } else {
            throw new \Exception('Configuration file [' . $filename . '] not found.', 500);
        }

        return false;
    }

}
