<?php

namespace MFrame;

/**
 * Core class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core
 */
class Core {

    /**
     * Instance of this class
     * @var object self
     */
    private static $_instance = null;

    /**
     * Is application loaded?
     * @var boolean 
     */
    private static $_loaded = false;

    /**
     * Minimal PHP version
     * @var int
     */
    private static $_min_phpver = 5.4;

    /**
     * Constructor
     * @param string $appPath Application path
     * @throws \Exception
     */
    private function __construct($appPath) {

        if (phpversion() < self::$_min_phpver) {
            throw new \Exception('PHP Version is too old. Please, install PHP ' . self::$_min_phpver . ' at least!', 500);
        }

        /*
         * Set constants
         */
        define('APP_PATH', realpath($appPath));
        define('PUBLIC_PATH', realpath(''));
        define('SYS_PATH', realpath(__DIR__));
        define('BASE_URI', str_replace($_SERVER['DOCUMENT_ROOT'], '', str_replace('\\', '/', PUBLIC_PATH)));
        define('BASE_URL', self::getProtocol() . $_SERVER['HTTP_HOST'] . BASE_URI);

        require_once SYS_PATH . '/AutoLoader.php';
        AutoLoader::init();

        AutoLoader::setNamespace('MFrame', SYS_PATH);
        AutoLoader::setNamespace('Models', APP_PATH . '/models');
        AutoLoader::setNamespace('Controllers', APP_PATH . '/controllers');
        set_exception_handler(__NAMESPACE__ . '\ErrorHandling::catchExceptions');

        Loader::loadFile(SYS_PATH . '/Helpers.php');

        if (Config::get('sessions.autoload')) {
            Sessions\Session::getInstance();
        }
        self::_callHooks();
        Routing::init();
    }

    /**
     * It looks for hooks in the configuration and if there are - it loads them.
     * @throws \Exception
     */
    private static function _callHooks() {
        $cnf = Config::get('hooks', []);
        foreach ($cnf as $hook) {
            if (!is_callable($hook['method'])) {
                throw new \Exception('Hook [' . $hook['method'] . '] not found.', 500);
            }

            call_user_func_array($hook['method'], is_array($hook['attr']) ? $hook['attr'] : []);
        }
    }

    /**
     * Get instance of this class
     * @return object self
     */
    public static function getInstance($appPath) {
        if (!self::$_instance) {
            self::$_instance = new \MFrame\Core($appPath);
        }
        return self::$_instance;
    }

    /**
     * Run application
     * @throws \Exception
     */
    public static function run() {
        if (!self::$_loaded) {
            Routing::display();
            self::$_loaded = true;
        } else {
            throw new \Exception('MFrame already working', 500);
        }
    }

    /**
     * Fix path
     * 
     * @param string $path Path
     * @return string
     */
    public static function fixPath($path) {
        $path = str_replace(array('\\', '/', DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR), DIRECTORY_SEPARATOR, $path);

        if (strpos($path, DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR)) {
            $path = Core::fixPath($path);
        }
        return $path;
    }

    public static function getProtocol() {
        return ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    }

}
