<?php

namespace MFrame;

/**
 * Loader class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2014 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core
 */
class Loader {

    /**
     * Load file
     * 
     * @param string $path File path
     * @return boolean
     * @throws \Exception
     */
    public static function loadFile($path) {
        $path = self::fixPath($path);

        if (file_exists($path)) {
            include $path;
            return true;
        }
        throw new \Exception('File [' . $path . '] cannot be loaded.', 500);
    }
    
    /**
     * Creates an isntance of a class
     * 
     * @param string $class Class name
     * @return boolean|object
     */
    public static function loadClass($class) {
        if(class_exists($class)) {
            return new $class();
        }
        
        return false;
    }

    /**
     * Fix path
     * 
     * @param string $path Path
     * @return string
     */
    public static function fixPath($path) {
        $path = str_replace(array('\\', '/', DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR), DIRECTORY_SEPARATOR, $path);

        if (strpos($path, DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR)) {
            $path = Core::fixPath($path);
        }
        return $path;
    }

}
