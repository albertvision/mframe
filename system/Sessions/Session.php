<?php

namespace MFrame\Sessions;

/**
 * Session class
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core.Sessions
 */
class Session {

    /**
     *
     * @var object self 
     */
    private static $_instance = null;

    /**
     * Drivers
     * @var type 
     */
    private static $_drivers = array('native' => '__NAMESPACE__\NativeSession', 'database' => '__NAMESPACE__\DBSession');

    /**
     *
     * @var object \MFrame\Sessions\ISession 
     */
    private static $_session = null;

    /**
     * Temporary cookie
     * @var string
     */
    private static $_tmp = null;

    /**
     * Session initialization
     * @return boolean
     * @throws \Exception
     */
    private function __construct() {
        $_config = \MFrame\Config::get('sessions');

        if (trim($_config['driver'])) {

            /*
             * Get session driver
             */
            if ($_config['custom_drivers'][$_config['driver']]) {
                $_class = $_config['custom_drivers'][$_config['driver']];
            } elseif (self::$_drivers[$_config['driver']]) {
                $_class = self::$_drivers[$_config['driver']];
            }
            $_class = str_replace('__NAMESPACE__', __NAMESPACE__, $_class);

            /*
             * If class exists
             */
            if (class_exists($_class)) {
                $_object = new $_class();

                /*
                 * If class implements \MFrame\Sessions\ISession
                 */
                if ($_object instanceof ISession) {

                    /*
                     * Set session name if there isn't
                     */
                    if (!strlen($_config['sess_name'])) {
                        $_config['sess_name'] = '_sess';
                    }

                    /*
                     * Creates temporary cookie
                     */
                    if ($_config['create_tmp']) {
                        if (!$_COOKIE['_tmp_sess']) {
                            self::$_tmp = sha1(time() . microtime() . $_SERVER['REMOTE_ADDR'] . '_tmp_sess');
                            setcookie('_tmp_sess', self::$_tmp, 0, $_config['path'], $_config['domain'], $_config['secure'], true);
                        } elseif ($_COOKIE['_tmp_sess']) {
                            self::$_tmp = $_COOKIE['_tmp_sess'];
                        }
                    } elseif($_COOKIE['_tmp_sess']) {
                        setcookie('_tmp_sess', self::$_tmp, time()-3600, $_config['path'], $_config['domain'], $_config['secure'], true);
                    }

                    /*
                     * Start session
                     */
                    self::$_session = $_object;
                    self::$_session->start($_config['sess_name'], $_config['max_life_time'], $_config['path'], $_config['domain'], $_config['secure']);

                    /*
                     * Flush old sessions
                     */
                    if (rand(0, 100) == 1) {
                        self::$_session->gc();
                    }

                    return true;
                } else {
                    throw new \Exception('Session driver [' . $_config['driver'] . '] doesn\'t implements ' . __NAMESPACE__ . '\ISession interface');
                }
            }
        }
        throw new \Exception('Session driver [' . $_config['driver'] . '] not found.');
    }

    /**
     * Get instance
     * @return object self
     */
    public static function getInstance() {
        if (!self::$_instance) {
            self::$_instance = new Session;
        }

        return self::$_instance;
    }

    /**
     * Get session data
     * @return type
     */
    public static function all() {
        return self::$_session->getAll();
    }

    /**
     * Get session value by notation
     * @param null|string $notation Notation
     * @return mixed
     */
    public static function get($notation = null) {
        if (!trim($notation)) {
            return self::$_session->getAll();
        }
        $parse = explode('.', $notation, 2);
        return array_get($parse[1], self::$_session->get($parse[0]));
    }

    /**
     * Get temporary cookie
     * @return string|null
     */
    public static function getTmp() {
        if (self::$_tmp) {
            return self::$_tmp;
        }
        throw new \Exception('Temporary cookie is not on.');
    }

    /**
     * Set value for session by notatation
     * 
     * @param string $notation Session notation
     * @param mixed $value New value
     * 
     * @return boolean
     */
    public static function set($notation, $value) {
        self::$_session->setAll(array_set($notation, $value, self::$_session->getAll()));
        return true;
    }

    /**
     * Session exists?
     * @param string $notation Session notation
     * 
     * @return boolean
     */
    public static function has($notation) {
        return (bool) self::get($notation);
    }

    /**
     * Remove session data
     * @param string $notation Notation
     * 
     * @return boolean
     */
    public static function remove($notation) {
        return self::set($notation, NULL);
    }

    /**
     * Destroy session
     */
    public static function destroy() {
        self::$_session->destroy();
    }

    /**
     * Save session data
     */
    public function __destruct() {
        self::$_session->saveSession();
    }

}
