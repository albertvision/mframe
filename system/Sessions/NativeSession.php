<?php

namespace MFrame\Sessions;

/**
 * Native session driver
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core.Sessions
 */
class NativeSession implements ISession {
    
    /**
     * Start session
     * @param string $name Session name
     * @param int $lifetime Session lifetime
     * @param string $path Session path
     * @param string $domain Session domain
     * @param boolean $secure Secured session?
     * @return boolean 
     */
    public function start($name, $lifetime, $path, $domain, $secure) {
        session_set_cookie_params($lifetime, $path, $domain, $secure, true);
        session_name($name);
        
        return session_start();
    }
    
    /**
     * Destroy session
     */
    public function destroy() {
        $this->setAll([]);
        session_destroy();
    }

    /**
     * Get session data
     * @param string $name Session name
     * @return mixed
     */
    public function get($name) {
        return $_SESSION[$name];
    }
    
    /**
     * Get all data
     * @return array
     */
    public function getAll() {
        return $_SESSION;
    }
    
    /**
     * Set new data
     * @param array $value New data
     */
    public function setAll($value) {
        $_SESSION = $value;
    }

    /**
     * Garbage collector
     */
    public function gc() { }
    
    /**
     * Save session data
     */
    public function saveSession() {
        session_write_close();
    }

}