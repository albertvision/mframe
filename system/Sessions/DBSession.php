<?php

namespace MFrame\Sessions;

/**
 * Database session driver
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core.Sessions
 */
class DBSession implements ISession {

    /**
     * Database instance
     * @var object
     */
    private $_db;
    
    /**
     * Database table
     * @var string
     */
    private $_dbTable;
    
    /**
     * Session data
     * @var array
     */
    private $_data = [];
    
    /**
     * Original data
     * @var array
     */
    private $_original_data = [];
    
    /**
     * Session key
     * @var type 
     */
    private $_key;

    /**
     * Get DB table
     */
    public function __construct() {
        $dbData = explode('.', \MFrame\Config::get('sessions.db_table'), 2);
        
        $this->_dbTable = $dbData[1];
        $this->_db = \MFrame\Libraries\DB::connect($dbData[0]);
    }

    /**
     * Destroy session
     */
    public function destroy() {
        $this->_data = [];
    }
    
    /**
     * Garbage collector
     * @return boolean
     */
    public function gc() {
        $to_expire = time()+\MFrame\Config::get('sessions.max_life_time');
        return $this->_db->query('DELETE FROM `'.$this->_dbTable.'` WHERE `expires` > FROM_UNIXTIME(?) AND `expires` != 0', array($to_expire));
    }

    /**
     * Get data
     * @param string $name
     * @return mixed
     */
    public function get($name) {
        return $this->_data[$name];
    }

    /**
     * Get all data
     * @return array
     */
    public function getAll() {
        return $this->_data;
    }

    /**
     * Set new data
     * @param array $value New data
     */
    public function setAll($value) {
        $this->_data = $value;
    }

    /**
     * Start session
     * @param string $name Session name
     * @param int $lifetime Session lifetime
     * @param string $path Session path
     * @param string $domain Session domain
     * @param boolean $secure Secured session?
     * @return boolean
     */
    public function start($name, $lifetime, $path, $domain, $secure) {
        $to_expire = time()+$lifetime;
        
        if($lifetime == 0) {
            $to_expire = 0;
        }
        
        $query = $this->_db->query('SELECT * FROM `' . $this->_dbTable . '` WHERE `key`=? AND (`expires` >= NOW() OR `expires` = FROM_UNIXTIME(0))', array($_COOKIE[$name]));
        
        if(!$query->numRows()) {
            $key = md5(microtime() . $_SERVER['REMOTE_ADDR']);
            $this->_db->query('INSERT INTO `'.$this->_dbTable.'` (`key`, `data`, `expires`, `added`) VALUES(?, ?, FROM_UNIXTIME(?), NOW())', array($key, 'a:0:{}', $to_expire));
            setcookie($name, $key, $to_expire, $path, $domain, $secure, true);
        } else {
            $key = $_COOKIE[$name];
        }
        
        $this->_key = $key;
        $this->_getSession();
        
        return true;
    }
    
    /**
     * Get session data
     * @return type
     */
    private function _getSession() {
        if(!$this->_data) {
            $row = $this->_db->query('SELECT `data` FROM `' . $this->_dbTable . '` WHERE `key`=? AND `expires` < FROM_UNIXTIME(?) AND `expires` != 0', array($this->_key, time()+\MFrame\Config::get('sessions.max_life_time')))->fetchRowAssoc();
            $data = unserialize($row['data']);
            
            $this->_data = is_array($data) ? $data : [];
            $this->_original_data = $this->_data;
            
        }
        return $this->_data;
    }
    
    /**
     * Save session
     * @return boolean
     */
    public function saveSession() {
        if($this->_key && $this->_data != $this->_original_data) {
            return $this->_db->query('UPDATE `'.$this->_dbTable.'` SET `data`=? WHERE `key`=?', array(serialize($this->_data), $this->_key));
        }
        return false;
    }
}
