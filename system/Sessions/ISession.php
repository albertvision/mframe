<?php

namespace MFrame\Sessions;

/**
 * Session interface
 *
 * @author Yasen Georgiev <avbincco@gmail.com>
 * @link http://framework.maleeby.com/
 * @copyright Copyright &copy; 2013 Yasen Georgiev
 * @license http://framework.maleeby.com/#license
 * @package Core.Sessions
 */
interface ISession {
    /**
     * Destroy session
     */
    public function destroy();
    
    /**
     * Garbage collector
     */
    public function gc();
    
    /**
     * Get session data
     * @param string $name Session name
     * @return mixed
     */
    public function get($name);

    /**
     * Get all data
     * @return array
     */
    public function getAll();
    
    /**
     * Set new data
     * @param array $value New data
     */
    public function setAll($value);
    
    /**
     * Save session data
     */
    public function saveSession();
    
    /**
     * Start session
     * @param string $name Session name
     * @param int $lifetime Session lifetime
     * @param string $path Session path
     * @param string $domain Session domain
     * @param boolean $secure Secured session?
     * @return boolean 
     */
    public function start($name, $lifetime, $path, $domain, $secure);
}
