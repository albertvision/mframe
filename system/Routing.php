<?php

namespace MFrame;

class Routing {

    private static $_uriData = array();
    private static $_initialized = false;
    private static $_uri;
    private static $_defaultConfig = [];
    private static $_config = [
        'routes' => [],
        'custom_regex' => [],
        'defaults' => []
    ];

    private function __construct() {
        
    }

    /**
     * Initialization of the Routing class.
     * 
     * @throws \Exception
     */
    public static function init() {
        if (!self::$_initialized) {
            self::_initConfig();
            self::getURI();
            self::$_initialized = true;
            self::$_uriData = self::_callDisplatcher(self::$_config['dispatcher']); // It looks for the dispatcher and returns its data
        }
    }

    /**
     * Call the route
     * @throws \Exception
     */
    public static function display() {
        if (class_exists(self::$_uriData['controller'])) {
            $controller = new self::$_uriData['controller']();
        }

        /*
         * If method exists
         */
        if (is_callable(array($controller, self::$_uriData['method'])) && self::$_uriData['method'][0] != '_') {
            /*
             * Set params
             */
            call_user_func_array(array($controller, self::$_uriData['method']), self::$_uriData['params']);
        } elseif (method_exists($controller, '_methodNotFound')) {
            call_user_func_array(array($controller, '_methodNotFound'), [self::$_uriData['method'], self::$_uriData['params']]);
        } else {
            throw new \Exception('Method [' . self::$_uriData['method'] . '@' . self::$_uriData['controller'] . '] </b> not found', 404);
        }
    }

    /**
     * Calls the routing dispatcher and checks about its validity
     * 
     * @param string $callback The Dispatcher's callback
     * @return array
     * 
     * @throws \Exception
     */
    private static function _callDisplatcher($callback) {
        if (!is_callable($callback)) {
            throw new \Exception('Routing displatcher [' . $callback . '] not found.', 500);
        }

        $result = call_user_func($callback);
        if (!is_string($result['controller']) || !is_string($result['method']) || !is_array($result['params'])) {
            throw new \Exception('Invalid dispatcher [' . $callback . ']');
        }
        return $result;
    }

    /**
     * It initalizes the Routes' configuration
     * @return array The configuration
     */
    private static function _initConfig() {
        if (!self::$_defaultConfig) {
            self::$_defaultConfig = Config::get('routing');
            self::$_config = array_merge_recursive(self::$_config, self::$_defaultConfig);
        }

        return self::$_config;
    }

    /**
     * Adds a custom route.
     * 
     * @param string $route Route URI
     * @param string $callback Route's callback
     * @param array $customRegex
     */
    public static function add($route, $callback, $customRegex = []) {
        self::$_config['routes'][$route] = $callback;
        self::$_config['custom_regex'] = array_merge(self::$_config['custom_regex'], $customRegex);
    }

    /**
     * This is the default routing dispather. It gets the controllers, methods and parameters from the requested URI.
     * 
     * @return array
     */
    public static function mainDispatcher() {
        $uri = self::getURI();
        $config = self::$_config;

        foreach ($config['routes'] as $uriPattern => $route) {
            $regex = self::getRegexPattern($uriPattern);

            if (preg_match($regex, self::$_uri . '/', $result)) {
                $uri = preg_replace($regex, $route, self::$_uri . '/');
                break;
            }
        }
        $controllerData = self::_getControllerDataByURI($uri);
        return self::parseURI($controllerData['uri'], $controllerData['defaults'], $controllerData['namespace']);
    }

    /**
     * Returns the controller's data for an URI
     * 
     * @param string $uri URI
     * @param string $namespace URI namespace
     * 
     * @return array [string uri, string namespace, array defaults]
     */
    private static function _getControllerDataByURI($uri, $namespace = 'Controllers') {
        $dir = Loader::fixPath(APP_PATH . '/' . lcfirst($namespace) . '/');
        if (!is_dir($dir . $uri)) {
            $uriParts = explode('/', $uri, 2);
            if (is_dir($dir . ucfirst(strtolower($uriParts[0])))) {
                return self::_getControllerDataByURI($uriParts[1], $namespace . '\\' . ucfirst(strtolower($uriParts[0])));
            }
        } else {
            $namespace = $namespace . '\\' . str_replace('/', '\\', $uri);
            $uri = '';
        }

        if (substr($namespace, -1) == '\\') {
            $namespace = substr($namespace, 0, strlen($namespace) - 1);
        }

        return [
            'uri' => $uri,
            'namespace' => $namespace,
            'defaults' => self::subControllerDefaults($namespace)
        ];
    }

    /**
     * Returns default controller/method set in app/config/routing.php
     * 
     * @param string $namespace Namespace of the controller
     * @return array Default method and controller
     */
    public static function subControllerDefaults($namespace) {
        if (substr($namespace, 0, 12) == 'Controllers\\') {
            $namespace = substr($namespace, 12);
        }

        $defaults = self::$_config['defaults'];
        $return = $defaults[strtolower(str_replace('\\', '/', $namespace))];

        if (!$return['controller']) {
            $return['controller'] = $defaults['*']['controller'];
        }
        if (!$return['method']) {
            $return['method'] = $defaults['*']['method'];
        }

        return $return;
    }

    /**
     * Returns a RegEX pattern of a URI pattern
     * 
     * @param string $uriPattern
     * @return string A RegEX
     */
    public static function getRegexPattern($uriPattern) {
        $defaultRegex = '\w+';
        $customRegex = self::$_config['custom_regex'];
        $regex = $uriPattern;

        preg_match_all('#\((\w+)\??\)#', $uriPattern, $matches); //Looking for a parameters.

        foreach ($matches[0] as $key => $value) {
            // If parameter has a custom RegEX
            if ($customRegex[$matches[1][$key]]) {
                $replacementRegex = $customRegex[$matches[1][$key]];
            } else {
                $replacementRegex = $defaultRegex;
            }

            //Optional parameter
            if (substr($value, -2) == '?)') {
                $replacementRegex .= '?';
            }

            $regex = str_replace($value, "($replacementRegex)", $regex);
        }

        return '#^' . $regex . '#';
    }

    /**
     * Parses an URI
     * @return array
     */
    public static function parseURI($uri = null, $defaults = null, $namespace = 'Controllers') {
        $uri = parse_url(clearURL($uri))['path'];
        $parsed = explode('/', $uri);

        if (!$defaults) {
            $defaults = self::$_config['defaults']['*'];
        }

        /*
         * Get controller
         */
        $controller = $defaults['controller']; // Default controller
        if (trim($parsed[0])) {
            $controller = $parsed[0];
        }

        /*
         * Get method
         */
        $method = $defaults['method']; // Default method
        if (trim($parsed[1])) {
            $method = $parsed[1];

            /*
             * Get attributes
             */
            if (count($parsed) > 2) {
                unset($parsed[0], $parsed[1]);

                $params = array_values($parsed);
            }
        }

        return array(
            'controller' => $namespace . '\\' . ucfirst($controller).Config::get('routing.controllers_suffix', null),
            'method' => $method,
            'params' => $params ? $params : []
        );
    }

    /**
     * Get current route
     * 
     * @param boolean $full To return the full route (with params)?
     * @return string
     */
    public static function getRoute($full = false) {
        $return = self::$_uriData['controller'] . '@' . self::$_uriData['method'];

        if ($full && count(self::$_uriData['params'])) {
            $return .= '@' . implode('@', self::$_uriData['params']);
        }
        return $return;
    }

    /**
     * Get current URI
     * 
     * bool type $queries Get queries?
     * @return type
     */
    public static function getURI($queries = true) {
        if (!self::$_uri) {
            $parsedScriptName = pathinfo($_SERVER['SCRIPT_NAME']);
            self::$_uri = mb_strtolower(clearURL(str_replace(array($parsedScriptName['basename'], $parsedScriptName['dirname'] . '/'), '', $_SERVER['REQUEST_URI'])));
        }

        return $queries ? self::$_uri : explode('?', self::$_uri)[0];
    }

    /**
     * Get current URI data
     * 
     * @return string
     */
    public static function getURIData() {
        return self::$_uriData;
    }

}
