<?php

namespace MFrame;

class Display {

    /**
     * Load view
     * @param string $file View name
     * @param array $data View data
     * @param boolean $returnString Return as string?
     * 
     * @return mixed
     * @throws \Exception
     */
    public static function view($file, $data = [], $returnString = false, $absolutePath = false) {
        if (!$absolutePath) {
            $filename = APP_PATH . '/views/' . $file . '.php';
        } else {
            $filename = $file . '.php';
        }
        $filename = Loader::fixPath($filename);

        if (!is_array($data)) {
            $data = [];
        }

        if (is_readable($filename) && is_file($filename)) {
            ob_start();
            extract($data);
            include $filename;

            $output = ob_get_clean();

            if ($returnString === TRUE) {
                return $output;
            } else {
                echo $output;
            }
        } else {
            throw new \Exception('View [' . $filename . '] not found.', 500);
        }
    }

    public static function json($array = []) {
        header('Content-type: text/json');

        echo json_encode($array);
        exit();
    }

    public static function xml($array = []) {
        header('Content-type: text/xml');
        echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        echo xml_encode($array);
        exit();
    }

}
