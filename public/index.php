<?php

error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
ini_set('max_execution_time', 0);

require_once '../system/Core.php';

\MFrame\Core::getInstance('../app');
\MFrame\Core::run();